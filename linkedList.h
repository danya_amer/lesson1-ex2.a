#ifndef STACK_H
#define STACK_H

/* a positive-integer value stack, with no size limit */
typedef struct stack
{
	int num;
	struct next* stack;
} stack;

void push(stack* s, unsigned int element);
int pop(stack* s); // Return -1 if stack is empty


#endif // STACK_H
