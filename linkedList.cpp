#define STACK_H

/* a positive-integer value stack, with no size limit */
typedef struct stack
{
	int num;
	struct stack* next;
} stack;

void push(stack* s, unsigned int element)
{
	stack* newstack = new stack;
	newstack->num = element;
	newstack->next = s;
	s = nullptr;
}

int pop(stack* s) // Return -1 if stack is empty
{
	if (s == nullptr)
		return -1;
	else
		s = s->next;
}
